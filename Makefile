# tools declarations
CROSS_COMPILE=arm-none-eabi-
CC=$(CROSS_COMPILE)gcc
#CC=clang
LD=$(CROSS_COMPILE)gcc
OBJCOPY=$(CROSS_COMPILE)objcopy
OBJDUMP=$(CROSS_COMPILE)objdump
SIZE=$(CROSS_COMPILE)size

# root build directory
BUILD = build

# call with `make DEBUG=1` to put debug info into elf file
DEBUG ?= 0

# directories with header files
INC = -I.
INC += -Icmsis/inc
INC += -Icmsis/devinc
INC += -Ihal/inc/

# compiler flags
CFLAGS += -msoft-float
ifeq ($(CC), clang)
CFLAGS += --target=thumbv7m--none-eabi
# option below is required for compatibility with newlib-nano
CFLAGS += -fshort-enums
else
CFLAGS += -march=armv7-m -mtune=cortex-m3 -mthumb -mno-thumb-interwork -mfix-cortex-m3-ldrd
endif
CFLAGS += $(INC)
CFLAGS += -DSTM32F103xB -DARM_MATH_CM3
CFLAGS += -Wall -Wno-parentheses-equality -Werror -std=gnu99
CFLAGS += -fdata-sections -ffunction-sections
CFLAGS += $(COPT)

# linker flags
LDFLAGS += -nostartfiles
LDFLAGS += -Lcmsis/linker -Tstm32f103c8t6.ld
LDFLAGS += -Wl,-Map=$(@:.elf=.map) -Wl,--cref
LDFLAGS += -Wl,--gc-sections
# use newlib-nano
LDFLAGS += -mcpu=cortex-m3 -mthumb --specs=nano.specs
# or if you prefer your own implementation of standard library
# LDFLAGS += -nostdlib
LDFLAGS += -Lcmsis/lib
LIBS = -larm_cortexM3l_math

# compiler optimizations
ifeq ($(DEBUG), 1)
CFLAGS += -g -DUSE_FULL_ASSERT
ifeq ($(findstring gcc,$(CC)), gcc)
COPT += -Og
else
COPT += -O0
endif
else
COPT += -Os
endif

# HAL drivers source files
SRC_HAL = $(addprefix hal/src/stm32f1xx_,\
	hal_adc.c \
	hal_adc_ex.c \
	hal.c \
	hal_cortex.c \
	hal_crc.c \
	hal_dma.c \
	hal_flash.c \
	hal_flash_ex.c \
	hal_gpio.c \
	hal_gpio_ex.c \
	hal_i2c.c \
	hal_iwdg.c \
	hal_pwr.c \
	hal_rcc.c \
	hal_rcc_ex.c \
	hal_rtc.c \
	hal_rtc_ex.c \
	hal_spi.c \
	hal_spi_ex.c \
	hal_tim.c \
	hal_tim_ex.c \
	hal_uart.c \
	hal_usart.c \
	hal_wwdg.c \
	)

# Assembly source files
SRC_S = \
	cmsis/src/startup_stm32f103xb.S \

# C source files
SRC_C = \
	main.c \
	syscalls.c \
	uart.c \
	cmsis/src/system_stm32f1xx.c \
	stm32f1xx_it.c \
	stm32f1xx_hal_msp.c \

# create list of all object files
OBJ =
OBJ += $(addprefix $(BUILD)/, $(SRC_HAL:.c=.o))
OBJ += $(addprefix $(BUILD)/, $(SRC_C:.c=.o))
OBJ += $(addprefix $(BUILD)/, $(patsubst %.S,%.o,$(SRC_S:.s=.o)))

# list of all directories needed to build
OBJ_DIRS = $(sort $(dir $(OBJ)))

OUTFILES = $(addprefix $(BUILD)/, \
	firmware.elf \
	firmware.dmp \
	firmware.hex \
	firmware.bin \
	)

### BUILD RULES ###

# create target files
all: $(OUTFILES)

# flash binary image using st-flash tool
# https://github.com/texane/stlink
flash: $(BUILD)/firmware.bin
	@echo "FLASH " $^
	@st-flash write $^ 0x08000000

# remove build directory
clean:
	rm -rf $(BUILD)

# executable linking
$(BUILD)/firmware.elf: $(OBJ)
	@echo "LD " $@
	@$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)
	@$(SIZE) $@

# executable file dump
$(BUILD)/firmware.dmp: $(BUILD)/firmware.elf
	@echo "DP " $@
	@$(OBJDUMP) -x --syms $< > $@

# intel hex for some programmers
$(BUILD)/firmware.hex: $(BUILD)/firmware.elf
	@echo "CP " $@
	@$(OBJCOPY) -O ihex $< $@

# raw binary image, represents chip flash memory (@0x08000000)
$(BUILD)/firmware.bin: $(BUILD)/firmware.elf
	@echo "CP " $@
	@$(OBJCOPY) -O binary $< $@

# C source file compilation
$(BUILD)/%.o: %.c
	$(call compile)

# Assembly source file compilation
$(BUILD)/%.o: %.s
	$(call compile)

# Assembly source file compilation (with preprocessing)
$(BUILD)/%.o: %.S
	$(call compile)

# object files need their directories
$(OBJ): | $(OBJ_DIRS)

# create build directories
$(OBJ_DIRS):
	@mkdir -p $@

## helper functions

define compile
@echo "CC " $<
@$(CC) -c $(CFLAGS) -o $@ $<
endef
