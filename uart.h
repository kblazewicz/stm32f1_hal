void uart_init(void);
int uart_write(uint8_t no, uint8_t *p, int len);
int uart_read(uint8_t no, uint8_t *p, int len);

// typedef struct _uart_t {
//   uint8_t no;
//   uint8_t buf[256];
//   uint8_t head, tail;
// } uart_t;
//
// extern uart_t uart2;
