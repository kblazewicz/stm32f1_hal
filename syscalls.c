#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdint.h>

#include "uart.h"

caddr_t _sbrk (int size)
{
  extern char _heap_start;
  extern char _heap_end;
  static char *current_heap_end = &_heap_start;
  char *previous_heap_end;

  previous_heap_end = current_heap_end;

  if (current_heap_end + size > &_heap_end) {
    errno = ENOMEM;
    return (caddr_t) -1;
  }

  current_heap_end += size;

  return (caddr_t) previous_heap_end;
}

ssize_t _write(int fd, const void *buf, size_t count)
{
  int res;
  switch (fd) {
    case STDOUT_FILENO:
      res = uart_write(2, (uint8_t*)buf, count);
      if (res < 0) {
        errno = EIO;
        return -1;
      }
      return res;

    default:
      errno = EBADF;
      return -1;
  }
}

ssize_t _read(int fd, const void *buf, size_t count)
{
  int res;
  switch (fd) {
    case STDIN_FILENO:
      res = uart_read(2, (uint8_t*)buf, count);
      if (res < 0) {
        errno = EIO;
        return -1;
      }
      return res;

    default:
      errno = EBADF;
      return -1;
  }
}

int _open(const char *pathname, int flags, ...)
{
  return -1;
}

int _close(int fd)
{
  return 0;
}

int _isatty(int fd)
{
  switch (fd) {
    case STDOUT_FILENO:
      return 1;

    default:
      errno = EBADF;
      return 0;
  }
}

off_t _lseek(int fd, off_t offset, int whence)
{
  errno = EBADF;
  return (off_t) -1;
}

int _fstat(int fd, struct stat *buf)
{
  switch (fd) {
    case STDOUT_FILENO:
    case STDIN_FILENO:
      buf->st_mode = S_IFCHR;
      return 0;

    default:
      errno = EBADF;
      return -1;
  }
}
