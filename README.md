## Prerequisites

### Toolchain
To compile project you will need *arm-none-eabi-gcc* and friends from [here](https://launchpad.net/gcc-arm-embedded/).

### Programming
For programming you can use ST-Link/V2 (any STM Nucleo board will do) programmer and *st-flash* software from [this project](https://github.com/texane/stlink).

## Build and Deployment
To build project simply run `make`, optionally add something like `-j4` if you
have multi-core CPU. After that you will have *firmware.elf*, *firmware.hex* and
*firmware.bin* files in *build* directory.

To cleanup run `make clean`

To program device run `make flash`
