#include "stm32f1xx_hal.h"
#include "uart.h"

USART_HandleTypeDef UartHandle;

// uart_t uart2;

void uart_init(void)
{
    UartHandle.Instance        = USART2;
    UartHandle.Init.BaudRate   = 115200;
    UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
    UartHandle.Init.Parity     = UART_PARITY_NONE;
    UartHandle.Init.StopBits   = UART_STOPBITS_1;
    UartHandle.Init.Mode       = UART_MODE_TX_RX;

    if (HAL_USART_Init(&UartHandle) != HAL_OK) {
        while (1);
    }
}

int uart_write(uint8_t no, uint8_t *p, int len)
{
  switch (no) {
    case 2:
      if (HAL_USART_Transmit(&UartHandle, p, len, 0xFFFF) == HAL_OK) {
        return len;
      } else {
        return -1;
      }

    default:
      return -1;
  }
}

int uart_read(uint8_t no, uint8_t *p, int len)
{
  return -1;
}
